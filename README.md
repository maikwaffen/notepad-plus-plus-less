# Notepad Sublime Syntax Highlighting for less #

The initial version is from [LESS for Notepad++ by Giuseppe Di Mauro](https://github.com/azrafe7/LESS-for-Notepad-plusplus).
This Syntax Highlighting is adapted for me.

## Install ##

* download the **XML** file
* open Nodepad++
* go to Language -> Define your language
* klick Import and select the less.xml file
* restart Nodepad++

## Base Colors ##

| Name       | RGB Values          | HEX Values |
|------------|---------------------|------------|
| gray-dark  | rgb(39, 40, 34)     | #272822    |
| gray-light | rgb(192, 192, 192)  | #c0c0c0    |
| gray       | rgb(117, 113, 94)   | #75715e    |
| blue       | rgb(102, 217, 239)  | #66d9ef    | 
| green      | rgb(166, 226, 46)   | #a6e22e    |
| pink       | rgb(249, 38, 114)   | #f92672    |
| orange     | rgb(253, 115, 31)   | #fd731f    |
| yellow     | rgb(230, 219, 116)  | #e6db74    |
| purple     | rgb(174, 129, 255)  | #ae81ff    |
| dark-green | rgb(0,139,139)      | #008b8b    |
